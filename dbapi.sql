-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbapi
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_auth`
--
create database dbapi;
use dbapi;

DROP TABLE IF EXISTS `api_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_auth` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token_id` int DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_auth`
--

LOCK TABLES `api_auth` WRITE;
/*!40000 ALTER TABLE `api_auth` DISABLE KEYS */;
INSERT INTO `api_auth` (`id`, `token_id`, `group_id`) VALUES (1,1,'YXO1K7q4'),(6,3,'428490ce-fa32-48d4-accc-a9f6401db5ef');
/*!40000 ALTER TABLE `api_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_config`
--

DROP TABLE IF EXISTS `api_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_config` (
  `id` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `params` text,
  `status` int DEFAULT NULL,
  `datasource_id` varchar(255) DEFAULT NULL,
  `previlege` int DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `cache_plugin` varchar(255) DEFAULT NULL,
  `cache_plugin_params` varchar(255) DEFAULT NULL,
  `transform_plugin` varchar(255) DEFAULT NULL,
  `transform_plugin_params` varchar(255) DEFAULT NULL,
  `create_time` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_config`
--

LOCK TABLES `api_config` WRITE;
/*!40000 ALTER TABLE `api_config` DISABLE KEYS */;
INSERT INTO `api_config` (`id`, `path`, `name`, `note`, `params`, `status`, `datasource_id`, `previlege`, `group_id`, `cache_plugin`, `cache_plugin_params`, `transform_plugin`, `transform_plugin_params`, `create_time`, `update_time`) VALUES ('0d3284b8-dc57-4f42-a5e2-f6060bbf5a47','getAllToken','名称','描述','[]',0,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',0,'YXO1K7q4',NULL,NULL,NULL,NULL,'2022-02-07 18:17:28','2022-02-07 18:17:28'),('506fba12-0c16-44f3-8316-778a56883429','ddddd','ddddd','dddddd','[{\"name\":\"sdagf\",\"type\":\"Array<bigint>\",\"note\":\"sdag\"}]',0,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',1,'428490ce-fa32-48d4-accc-a9f6401db5ef',NULL,NULL,NULL,NULL,'2022-02-15 12:18:26','2022-02-15 12:18:26'),('7b702378-b962-4839-a3b8-5ab2c02c6147','token','token','token','[]',0,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',0,'YXO1K7q4',NULL,NULL,NULL,NULL,'2022-02-07 18:17:28','2022-02-07 18:17:28'),('7ed6e5da-43f1-4dfb-a10d-27077012198f','addappi','addapi','addapi','[{\"name\":\"username\",\"type\":\"string\",\"note\":\"用户名称\"}]',0,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',1,'428490ce-fa32-48d4-accc-a9f6401db5ef',NULL,NULL,NULL,NULL,'2022-02-15 09:37:12','2022-02-15 09:37:12'),('90749f9e-ef3d-45ff-8ab2-1955d159ea20','p_test','param_test','param_test','[{\"name\":\"name\",\"type\":\"string\",\"note\":\"username\"}]',0,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',1,'YXO1K7q4',NULL,NULL,NULL,NULL,'2022-02-07 18:17:28','2022-02-07 18:17:28'),('be22a540-bf85-4664-942a-3cfa81d84aa4','pluginTest','pluginTest','pluginTest','[]',1,'92fe5035-44b0-44f3-8346-f14cb3bbc0e6',1,'428490ce-fa32-48d4-accc-a9f6401db5ef',NULL,NULL,'demo.EncryptTransformerPlugin','password','2022-02-09 17:49:08','2022-02-09 17:49:08');
/*!40000 ALTER TABLE `api_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_group`
--

DROP TABLE IF EXISTS `api_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_group` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_group`
--

LOCK TABLES `api_group` WRITE;
/*!40000 ALTER TABLE `api_group` DISABLE KEYS */;
INSERT INTO `api_group` (`id`, `name`) VALUES ('428490ce-fa32-48d4-accc-a9f6401db5ef','new'),('YXO1K7q4','test');
/*!40000 ALTER TABLE `api_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_sql`
--

DROP TABLE IF EXISTS `api_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_sql` (
  `id` int NOT NULL AUTO_INCREMENT,
  `api_id` varchar(64) NOT NULL,
  `sql_text` text NOT NULL,
  `transform_plugin` varchar(255) DEFAULT NULL,
  `transform_plugin_params` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_sql`
--

LOCK TABLES `api_sql` WRITE;
/*!40000 ALTER TABLE `api_sql` DISABLE KEYS */;
INSERT INTO `api_sql` (`id`, `api_id`, `sql_text`, `transform_plugin`, `transform_plugin_params`) VALUES (9,'be22a540-bf85-4664-942a-3cfa81d84aa4','SELECT * FROM user;',NULL,NULL),(10,'7ed6e5da-43f1-4dfb-a10d-27077012198f','Select * FROM user WHERE username = #{};',NULL,NULL),(11,'506fba12-0c16-44f3-8316-778a56883429','asdgfhgj',NULL,NULL);
/*!40000 ALTER TABLE `api_sql` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasource`
--

DROP TABLE IF EXISTS `datasource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datasource` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `driver` varchar(100) DEFAULT NULL,
  `table_sql` varchar(255) DEFAULT NULL,
  `create_time` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasource`
--

LOCK TABLES `datasource` WRITE;
/*!40000 ALTER TABLE `datasource` DISABLE KEYS */;
INSERT INTO `datasource` (`id`, `name`, `note`, `type`, `url`, `username`, `password`, `driver`, `table_sql`, `create_time`, `update_time`) VALUES ('1696e5f1-552d-4593-bbe9-df299ccd6938','sdfghsdfghjk','dfhgjh','SQL Server',NULL,NULL,NULL,'segdfhjkjljkjghfgdfs',NULL,'2022-02-13 21:13:19','2022-02-13 21:13:19'),('92fe5035-44b0-44f3-8346-f14cb3bbc0e6','sdgrdtfhgjh','localhost','mysql','jdbc:mysql://localhost:3306/dbapi?useSSL=false&characterEncoding=UTF-8&serverTimezone=GMT%2B8','root','','com.mysql.cj.jdbc.Driver','show tables','2022-01-17 18:04:28','2022-02-13 17:43:08'),('9f56ca5e-a1f0-4e84-9052-0b30f0d07e5a','datasourcenam','localhos','SQL Server','','','','','','2022-02-13 17:17:45','2022-02-13 21:08:34');
/*!40000 ALTER TABLE `datasource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firewall`
--

DROP TABLE IF EXISTS `firewall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `firewall` (
  `status` varchar(255) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firewall`
--

LOCK TABLES `firewall` WRITE;
/*!40000 ALTER TABLE `firewall` DISABLE KEYS */;
INSERT INTO `firewall` (`status`, `mode`) VALUES ('on','black');
/*!40000 ALTER TABLE `firewall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip_rules`
--

DROP TABLE IF EXISTS `ip_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ip_rules` (
  `type` varchar(255) DEFAULT NULL,
  `ip` varchar(10240) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip_rules`
--

LOCK TABLES `ip_rules` WRITE;
/*!40000 ALTER TABLE `ip_rules` DISABLE KEYS */;
INSERT INTO `ip_rules` (`type`, `ip`) VALUES ('white',NULL),('black','192.168.11.12');
/*!40000 ALTER TABLE `ip_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugin`
--

DROP TABLE IF EXISTS `plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plugin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT '插件名称',
  `note` text COMMENT '插件描述信息',
  `classname` varchar(256) NOT NULL COMMENT '插件的全类名',
  `param_desc` varchar(256) DEFAULT NULL COMMENT '插件参数',
  `files` varchar(512) DEFAULT NULL,
  `in_use` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='插件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugin`
--

LOCK TABLES `plugin` WRITE;
/*!40000 ALTER TABLE `plugin` DISABLE KEYS */;
INSERT INTO `plugin` (`id`, `name`, `note`, `classname`, `param_desc`, `files`, `in_use`) VALUES (3,'\"name\"','\"note\"','\"demo.EncryptTransformerPlugin\"','\"param_desc\"','E:\\desktop\\t\\DB-API\\plugin_files\\lib/c6365907-9003-4cfe-99ba-6fad50770ca8.jar;',0);
/*!40000 ALTER TABLE `plugin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `token` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `expire` bigint DEFAULT NULL,
  `create_time` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` (`id`, `token`, `note`, `expire`, `create_time`) VALUES (3,'d33d116b56dfc141de41326eea25e2b5','',1645372800000,1644220369229),(4,'05048cc51074e0e596d0d0e37a8ada2f',NULL,1645694317968,1644830322896),(6,'a41f81025dfdca9a9dadf1a3facaa080','带描述信息的token',1645878859330,1644842062257);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES (1,'admin','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-15 21:52:17
