package danran.dbapi.controller;

import com.alibaba.fastjson.JSON;
import danran.dbapi.common.ResponseDto;
import danran.dbapi.controller.util.OutputUtil;
import danran.dbapi.domain.DataSource;
import danran.dbapi.service.DataSourceService;
import danran.dbapi.utils.JDBCUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * @Classname DataSourceController
 * @Description TODO
 * @Date 2022/1/17 16:38
 * @Created by RanCoder
 */
@RestController
@RequestMapping("/datasource")
public class DataSourceController {
    private static final Logger log = LoggerFactory.getLogger(DataSourceController.class);
    @Autowired
    DataSourceService dataSourceService;

    @RequestMapping("/add")
    public void add(DataSource dataSource) {
        dataSourceService.add(dataSource);
    }

    @RequestMapping("/getAll")
    public List<DataSource> getAll() {
        return dataSourceService.getAll();
    }

    @RequestMapping("/detail/{id}")
    public DataSource detail(@PathVariable String id) {
        return dataSourceService.detail(id);
    }

    @RequestMapping("/delete/{id}")
    public ResponseDto delete(@PathVariable String id) {
        return dataSourceService.delete(id);
    }

    @RequestMapping("/update")
    public ResponseDto update(DataSource dataSource) {
        dataSourceService.update(dataSource);
        return ResponseDto.successWithMsg("更新成功");
    }

    @RequestMapping("/connect")
    public ResponseDto connect(DataSource dataSource) {
        Connection connection = null;
        try {
            connection = JDBCUtil.getConnection(dataSource);
            return ResponseDto.apiSuccess(null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseDto.fail(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    @RequestMapping("/export")
    public void export(String ids, HttpServletResponse response) {
        List<String> collect = Arrays.asList(ids.split(","));
        List<DataSource> list = dataSourceService.selectBatch(collect);
        String s = JSON.toJSONString(list);
        response.setContentType("application/x-msdownload;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=datasource.json");
        OutputUtil.fileOut(response, s);
    }


    @RequestMapping(value = "/import", produces = "application/json;charset=UTF-8")
    public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        String s = IOUtils.toString(file.getInputStream(), StandardCharsets.UTF_8);
        List<DataSource> list = JSON.parseArray(s, DataSource.class);
        dataSourceService.insertBatch(list);
    }

    @RequestMapping("/getName")
    public String getName(String id) {
        return dataSourceService.getDataSourceById(id).getName();
    }
}
