package danran.dbapi.controller;

import com.alibaba.fastjson.JSONArray;
import danran.dbapi.common.ResponseDto;
import danran.dbapi.domain.Group;
import danran.dbapi.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Classname GroupController
 * @Description TODO
 * @Date 2022/1/17 16:22
 * @Created by RanCoder
 */
@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    GroupService groupService;

    @RequestMapping("/create")
    public void createGroup(Group group) {
        groupService.insert(group);
    }

    @RequestMapping("/createGroups")
    public void createGroups(String groups) {
        System.out.println(groups);
        List<Group> groupList = Arrays.stream(groups.split(",")).map(Group::new).collect(Collectors.toList());
        for (Group group : groupList) {
            createGroup(group);
        }
    }

    @RequestMapping("/delete/{id}")
    public ResponseDto deleteGroup(@PathVariable String id) {
        return groupService.deleteById(id);
    }

    @RequestMapping("/getAll")
    public List<Group> getAll() {
        return groupService.getAll();
    }
}
