package danran.dbapi.controller;

import danran.dbapi.service.IPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Classname FirewallController
 * @Description TODO
 * @Date 2022/1/17 16:29
 * @Created by RanCoder
 */
@RestController
@RequestMapping("/firewall")
public class FirewallController {
    @Autowired
    IPService ipService;

    @RequestMapping("/save")
    public void save(String status, String mode, String whiteIP, String blackIP) {
        if ("on".equals(status)) {
            if ("white".equals(mode)) {
                ipService.on(mode, whiteIP);
            } else if ("black".equals(mode)) {
                ipService.on(mode, blackIP);
            }
        } else if (status.equals("off")) {
            ipService.off();
        }
    }

    @RequestMapping("/detail")
    public Map<String, String> detail() {
        return ipService.detail();
    }
}
