package danran.dbapi.controller.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @Classname OutputUtil
 * @Description TODO
 * @Date 2022/1/17 17:04
 * @Created by RanCoder
 */
public class OutputUtil {
    public static void fileOut(HttpServletResponse response, String s) {
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            os.write(s.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null)
                    os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
