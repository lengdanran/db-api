package danran.dbapi.controller.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname MyConfig
 * @Description TODO
 * @Date 2022/1/17 17:28
 * @Created by RanCoder
 */
@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Autowired
    JwtAuthInterceptor jwtAuthInterceptor;

    /**
     * 添加拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        List<String> patterns = new ArrayList<>();
        patterns.add("/user/login");
        patterns.add("/api/**");
        patterns.add("/js/**");
        patterns.add("/css/**");
        patterns.add("/fonts/**");
        patterns.add("/index.html");
        patterns.add("/error");

        registry.addInterceptor(jwtAuthInterceptor).excludePathPatterns(patterns).addPathPatterns("/**");
    }

}
