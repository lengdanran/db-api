package danran.dbapi.controller.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Classname CrossConfig
 * @Description TODO
 * @Date 2022/1/17 17:21
 * @Created by RanCoder
 */
@Configuration
public class CrossConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "HEAD", "OPTIONS", "DELETE", "PUT")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedMethods("*");
    }
}
