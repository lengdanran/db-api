package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.Group;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Classname GroupMapper
 * @Description TODO
 * @Date 2022/1/12 23:27
 * @Created by RanCoder
 */
@Mapper
public interface GroupMapper extends BaseMapper<Group> {
    @Select("select id from api_group;")
    List<String> getAllIds();
}
