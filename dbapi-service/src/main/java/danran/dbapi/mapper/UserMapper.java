package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @Classname UserMapper
 * @Description TODO
 * @Date 2022/1/12 23:34
 * @Created by RanCoder
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select * from user where username = #{username} and password = #{password} limit 1")
    User login(@Param("username") String username, @Param("password") String password);

    @Update("update user set password = #{password} where username = 'admin' ")
    void updatePassword(String password);

    @Update("update user set password = #{password} where username = #{name} ")
    void updatePassword(String name, String password);
}
