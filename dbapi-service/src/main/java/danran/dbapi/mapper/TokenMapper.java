package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.Token;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Classname TokenMapper
 * @Description TODO
 * @Date 2022/1/12 23:33
 * @Created by RanCoder
 */
@Mapper
public interface TokenMapper extends BaseMapper<Token> {

    @Select("select * from token where token = #{token}")
    Token selectByToken(String token);
}
