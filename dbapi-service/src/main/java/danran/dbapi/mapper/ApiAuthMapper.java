package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.ApiAuth;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Classname ApiAuthMapper
 * @Description TODO
 * @Date 2022/1/12 20:57
 * @Created by RanCoder
 */
@Mapper
public interface ApiAuthMapper extends BaseMapper<ApiAuth> {
    @Delete("delete from api_auth where token_id = #{tokenId}")
    void deleteByTokenId(Integer tokenId);

    @Select("select group_id from api_auth where token_id = #{tokenId}")
    List<String> selectByTokenId(Integer tokenId);

    @Delete("delete from api_auth where groupId = #{groupId}")
    void deleteByGroupId(String groupId);
}
