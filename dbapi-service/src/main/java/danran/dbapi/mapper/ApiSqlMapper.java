package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.common.ApiSql;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Classname ApiSqlMapper
 * @Description TODO
 * @Date 2022/1/12 23:07
 * @Created by RanCoder
 */
@Mapper
public interface ApiSqlMapper extends BaseMapper<ApiSql> {
    @Delete("delete from api_sql where api_id = #{id}")
    void deleteByApiID(String id);

    @Select("select * from api_sql where api_id = #{id}")
    List<ApiSql> selectByApiId(String id);
}
