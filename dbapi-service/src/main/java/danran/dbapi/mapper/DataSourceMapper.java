package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.DataSource;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Classname DataSourceMapper
 * @Description TODO
 * @Date 2022/1/12 23:16
 * @Created by RanCoder
 */
@Mapper
public interface DataSourceMapper extends BaseMapper<DataSource> {
}
