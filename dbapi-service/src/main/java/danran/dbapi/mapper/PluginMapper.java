package danran.dbapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import danran.dbapi.domain.Plugin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Classname PluginMapper
 * @Description TODO
 * @Date 2022/2/9 14:57
 * @Created by RanCoder
 */
@Mapper
public interface PluginMapper extends BaseMapper<Plugin> {
    @Update("update plugin set in_use = in_use + 1 where name = #{pluginName}")
    void addInUse(String pluginName);
    @Update("update plugin set in_use = in_use - 1 where name = #{pluginName}")
    void decreInUse(String pluginName);
}
