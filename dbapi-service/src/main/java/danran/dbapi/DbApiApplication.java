package danran.dbapi;

import danran.dbapi.common.ResponseDto;
import danran.dbapi.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname danran.dbapi.DbApiApplication
 * @Description TODO
 * @Date 2022/1/13 10:44
 * @Created by RanCoder
 */
@SpringBootApplication
@RestController
public class DbApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(DbApiApplication.class);
    }
    @Autowired
    UserMapper userMapper;
    @GetMapping("/get")
    public ResponseDto get() {
        return ResponseDto.successWithData(userMapper.login("admin", "admin"));
    }
}
