package danran.dbapi.service;

import danran.dbapi.domain.User;
import danran.dbapi.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Classname UserService
 * @Description TODO
 * @Date 2022/1/13 17:52
 * @Created by RanCoder
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public User getUser(String name, String password) {
        return userMapper.login(name, password);
    }

    public User getUserById(Integer id) {
        return userMapper.selectById(id);
    }

    @Transactional
    public void resetPassword(String password) {
        userMapper.updatePassword(password);
    }

    @Transactional
    public void resetPassword(String name, String password) {
        userMapper.updatePassword(name, password);
    }

    @Transactional
    public void insertUser(User user) {
        userMapper.insert(user);
    }
}
