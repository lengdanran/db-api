package danran.dbapi.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import danran.dbapi.common.ApiConfig;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Classname ApiService
 * @Description TODO
 * @Date 2022/1/13 15:02
 * @Created by RanCoder
 */
@Service
public class ApiService {
    /**
     * 从请求中获取sql的参数，并于apiConfig中的参数配置比对，返回param-value键值对
     *
     * @param req    请求
     * @param config 接口配置
     * @return 返回param-value键值对
     */
    public Map<String, Object> getSqlParam(HttpServletRequest req, ApiConfig config) {
        Map<String, Object> map = new HashMap<>();
        // 存储参数
        JSONArray reqParams = JSON.parseArray(config.getParams());
        if (reqParams == null) return map;
        for (int i = 0; i < reqParams.size(); i++) {
            JSONObject jsonObject = reqParams.getJSONObject(i);
            String name = jsonObject.getString("name");
            String type = jsonObject.getString("type");
            // 数组类型的参数
            if (type.startsWith("Array")) {
                String[] values = req.getParameterValues(name);
                if (values != null) {
                    List<String> vals = Arrays.asList(values);
                    if (vals.size() > 0) {
                        switch (type) {
                            case "Array<double>": {
                                List<Double> params = vals.stream().map(Double::valueOf).collect(Collectors.toList());
                                map.put(name, params);
                                break;
                            }
                            case "Array<bigint>": {
                                List<Long> params = vals.stream().map(Long::valueOf).collect(Collectors.toList());
                                map.put(name, params);
                                break;
                            }
                            case "Array<string>": {
                                map.put(name, vals.stream().map(String::valueOf).collect(Collectors.toList()));
                                break;
                            }
                            case "Array<date>": {
                                map.put(name, vals);
                                break;
                            }
                        }
                    } else {
                        map.put(name, vals);
                    }
                } else {
                    map.put(name, null);
                }
            } else { // 不是数组类型的参数
                String val = req.getParameter(name);
                if (!"".equals(val)) {
                    switch (type) {
                        case "double": {
                            map.put(name, Double.valueOf(val));
                            break;
                        }
                        case "bigint": {
                            map.put(name, Long.valueOf(val));
                            break;
                        }
                        case "string":
                        case "date": {
                            map.put(name, val);
                            break;
                        }
                    }
                } else {
                    map.put(name, val);
                }
            }
        }
        return map;
    }
}
