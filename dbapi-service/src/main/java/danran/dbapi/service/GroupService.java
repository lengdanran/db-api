package danran.dbapi.service;

import danran.dbapi.common.ResponseDto;
import danran.dbapi.domain.Group;
import danran.dbapi.mapper.ApiAuthMapper;
import danran.dbapi.mapper.ApiConfigMapper;
import danran.dbapi.mapper.GroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @Classname GroupService
 * @Description TODO
 * @Date 2022/1/13 16:28
 * @Created by RanCoder
 */
@Service
public class GroupService {
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private ApiConfigMapper apiConfigMapper;
    @Autowired
    private ApiAuthMapper apiAuthMapper;

    @Transactional
    public void insert(Group group) {
        group.setId(UUID.randomUUID().toString());
        groupMapper.insert(group);
    }

    @Transactional
    public ResponseDto deleteById(String id) {
        if (apiConfigMapper.selectCountByGroup(id) == 0) {
            groupMapper.deleteById(id);
            apiAuthMapper.deleteByGroupId(id);
            return ResponseDto.successWithMsg("删除成功");
        } else {
            return ResponseDto.fail("Group尚不为空，不能删除");
        }
    }

    public List<Group> getAll() {
        return groupMapper.selectList(null);
    }

    public List<Group> selectBatch(List<String> ids) {
        return groupMapper.selectBatchIds(ids);
    }

    public List<String> getAllGroupIds(){
        return groupMapper.getAllIds();
    }

    @Transactional
    public void insertBatch(List<Group> groups) {
        groups.forEach(g -> {
            groupMapper.insert(g);
        });
    }
}
