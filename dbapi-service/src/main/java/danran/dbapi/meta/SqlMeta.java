package danran.dbapi.meta;

import java.util.List;

/**
 * @Classname SqlMeta
 * @Description TODO
 * @Date 2022/1/14 11:43
 * @Created by RanCoder
 */
public class SqlMeta {
    private String sql;
    private List<Object> params;

    public SqlMeta() {
    }

    public SqlMeta(String sql, List<Object> params) {
        this.sql = sql;
        this.params = params;
    }

    public String getSql() {
        return sql;
    }

    public SqlMeta setSql(String sql) {
        this.sql = sql;
        return this;
    }

    public List<Object> getParams() {
        return params;
    }

    public SqlMeta setParams(List<Object> params) {
        this.params = params;
        return this;
    }
}
