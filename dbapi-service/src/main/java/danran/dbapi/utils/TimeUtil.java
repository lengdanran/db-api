package danran.dbapi.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Classname TimeUtil
 * @Description TODO
 * @Date 2022/1/13 11:46
 * @Created by RanCoder
 */
public class TimeUtil {
    public static String now() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
}
