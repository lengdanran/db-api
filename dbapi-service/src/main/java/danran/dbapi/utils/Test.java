package danran.dbapi.utils;

import java.sql.*;

/**
 * @Classname Test
 * @Description TODO
 * @Date 2022/1/17 10:57
 * @Created by RanCoder
 */
public class Test {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String url = "jdbc:mysql://127.0.0.1:3306/dbapi?useSSL=false&characterEncoding=UTF-8&serverTimezone=GMT%2B8";
        String username = "root";
        String password = "";
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(url, username, password);
        String sql = "SELECT * FROM datasource where 1=2";
        PreparedStatement prepareStatement = connection.prepareStatement(sql);
        ResultSetMetaData metaData = prepareStatement.executeQuery().getMetaData();
        System.out.println(metaData.getColumnCount());
        for (int i = 0; i < metaData.getColumnCount(); i++) {
            System.out.println(metaData.getColumnName(i + 1));
            System.out.println(metaData.getColumnClassName(i + 1));
            System.out.println("==========");
        }
//        ResultSet resultSet = prepareStatement.executeQuery();
//        resultSet.next();
//        String address = resultSet.getString("address");
//        System.out.println(address);
    }
}
