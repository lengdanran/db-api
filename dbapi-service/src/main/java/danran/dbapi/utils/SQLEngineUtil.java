package danran.dbapi.utils;

import danran.dbapi.core.engine.DynamicSqlEngine;

/**
 * @Classname SQLEngineUtil
 * @Description TODO
 * @Date 2022/1/14 15:50
 * @Created by RanCoder
 */
public class SQLEngineUtil {
    public static DynamicSqlEngine getEngine() {
        return new DynamicSqlEngine();
    }
}
