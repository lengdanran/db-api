package danran.dbapi.utils;

/**
 * @Classname Privilege
 * @Description TODO
 * @Date 2022/1/14 10:46
 * @Created by RanCoder
 */
public interface Privilege {
    int PUBLIC = 1;
    int PRIVATE = 0;
}
