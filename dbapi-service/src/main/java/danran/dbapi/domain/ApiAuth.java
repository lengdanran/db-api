package danran.dbapi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * api_auth数据库表实体对象
 * @Classname ApiAuth
 * @Description TODO
 * @Date 2022/1/12 20:59
 * @Created by RanCoder
 */
@TableName(value = "api_auth")
public class ApiAuth {
    @TableId(value = "id", type = IdType.AUTO)
    Integer id;
    @TableField("token_id")
    Integer tokenId;
    @TableField("group_id")
    String groupId;

    public ApiAuth() {
    }


    public ApiAuth(Integer id, Integer tokenId, String groupId) {
        this.id = id;
        this.tokenId = tokenId;
        this.groupId = groupId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTokenId() {
        return tokenId;
    }

    public ApiAuth setTokenId(Integer tokenId) {
        this.tokenId = tokenId;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
