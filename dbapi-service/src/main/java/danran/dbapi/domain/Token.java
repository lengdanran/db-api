package danran.dbapi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * @Classname Token
 * @Description TODO
 * @Date 2022/1/12 23:05
 * @Created by RanCoder
 */
@TableName(value = "token")
public class Token {

    @TableId(value = "id", type = IdType.AUTO)
    Integer id;

    @TableField
    String token;

    @TableField
    Long expire;

    @TableField
    String note;

    @TableField("create_time")
    Long createTime;
    public Token(){
    }

    public Token(Integer id, String token, Long expire, String note, Long createTime) {
        this.id = id;
        this.token = token;
        this.expire = expire;
        this.note = note;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getExpire() {
        return expire;
    }

    public void setExpire(Long expire) {
        this.expire = expire;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
