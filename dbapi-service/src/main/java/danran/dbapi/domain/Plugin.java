package danran.dbapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Classname Plugin
 * @Date 2022/2/9 14:58
 * @Created by RanCoder
 */
@TableName(value = "plugin")
public class Plugin {
    @TableId(value = "id")
    int id;
    @TableField
    String name;
    @TableField
    String note;
    @TableField
    String classname;
    @TableField
    String param_desc;
    @TableField
    String files;
    @TableField
    int in_use;

    public Plugin(int id, String name, String note, String classname, String param_desc, String files_path, int in_use) {
        this.id = id;
        this.name = name;
        this.note = note;
        this.classname = classname;
        this.param_desc = param_desc;
        this.files = files_path;
        this.in_use = in_use;
    }

    public Plugin() {
    }

    public int getIn_use() {
        return this.in_use;
    }

    public Plugin setIn_use(int in_use) {
        this.in_use = in_use;
        return this;
    }

    public int getId() {
        return id;
    }

    public Plugin setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Plugin setName(String name) {
        this.name = name;
        return this;
    }

    public String getNote() {
        return note;
    }

    public Plugin setNote(String note) {
        this.note = note;
        return this;
    }

    public String getClassname() {
        return classname;
    }

    public Plugin setClassname(String classname) {
        this.classname = classname;
        return this;
    }

    public String getParam_desc() {
        return param_desc;
    }

    public Plugin setParam_desc(String param_desc) {
        this.param_desc = param_desc;
        return this;
    }

    public String getFiles() {
        return files;
    }

    public Plugin setFiles(String files) {
        this.files = files;
        return this;
    }
}
