package danran.dbapi.domain;

/**
 * @Classname ApiDto
 * @Description TODO
 * @Date 2022/1/12 23:05
 * @Created by RanCoder
 */
public class ApiDto {
    String id;
    String name;
    String groupName;

    public ApiDto() {
    }

    public ApiDto(String id, String name, String groupName) {
        this.id = id;
        this.name = name;
        this.groupName = groupName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
