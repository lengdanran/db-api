package danran.dbapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * @Classname Group
 * @Description TODO
 * @Date 2022/1/12 23:05
 * @Created by RanCoder
 */
@TableName(value = "api_group")
public class Group {

    @TableId(value = "id")
    String id;

    @TableField
    String name;

    public Group() {
    }

    public Group(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public Group(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
