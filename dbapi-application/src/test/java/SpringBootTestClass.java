import danran.dbapi.plugin.PluginConf;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Classname SpringBootTestClass
 * @Description TODO
 * @Date 2022/2/8 11:30
 * @Created by RanCoder
 */
@SpringBootTest
public class SpringBootTestClass {

    @Test
    public void test_plugin_conf() {
        System.out.println(PluginConf.getKey("t"));
    }
}
