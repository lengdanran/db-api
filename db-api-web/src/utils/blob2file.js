export function save(blob, filename) {
  console.log(blob);
  // const blob = new Blob([res.data], {type: 'application/x-msdownload'});
  const alink = document.createElement('a');
  alink.download = filename;
  alink.style.display = 'none';
  alink.href = URL.createObjectURL(blob); // 这里是将文件流转化为一个文件地址
  document.body.appendChild(alink);
  alink.click();
  URL.revokeObjectURL(alink.href); // 释放URL 对象
  document.body.removeChild(alink);
}
