import React from 'react';
import { Menu, Layout } from 'antd';
import '../layouts/index.less';
import '../assets/incofont.css';
import { Link } from 'umi';
import avatar from '../assets/yay.jpg';
import { BellOutlined, SearchOutlined } from '@ant-design/icons';

function BasicLayout(props) {
  const { Sider, Content } = Layout;
  return (
    <div className="home">
      <Layout>
        <div className="header">
          <h1 className="title">DBApi</h1>
          <span className="subtitle">自由修改数据库</span>

          <span className="user-info">
            <SearchOutlined className="search" />
            <BellOutlined className="bell" />
            <img className="avatar" src={avatar} alt="user-avatar"></img>
            <span className="name">username</span>
          </span>
        </div>

        <Layout>
          <Sider>
            <Menu
              style={{ width: '200px', height: '100%' }}
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              mode="inline"
            >
              <Menu.Item key="sub1" icon={<i className="iconfont icon-xitongshezhi"></i>}>
                <Link to="/dataResource">数据源</Link>
              </Menu.Item>
              <Menu.Item key="sub2">
                <Link to="/apiManage">API管理</Link>
              </Menu.Item>
              <Menu.Item key="sub3">
                <Link to="/userPrivilege">权限配置</Link>
              </Menu.Item>
              <Menu.Item key="sub4">
                <Link to="/systemSetting">系统设置</Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Content className="content">{props.children}</Content>
        </Layout>
      </Layout>
    </div>
  );
}

export default BasicLayout;
