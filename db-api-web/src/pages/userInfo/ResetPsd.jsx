import React from 'react';
import './index.less';
import Rcoder from '../../assets/image/Rcoder.jpg';
import { Input, Button } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { Link } from 'umi';

export default function ResetPsd() {
  return (
    <div className="reset">
      <div className="box">
        <div className="logo">
          <img className="logo-img" src={Rcoder} alt="logoImage" />
          <span className="logo-word">DBApi</span>
        </div>
        <span className="login-font">重置密码</span>
        <Input size="large" placeholder="邮箱" prefix={<MailOutlined />} />
        <div className="send-line">
          <Button size="large" type="primary">
            发送重置邮件
          </Button>
          <span className="font">
            <Link className="to-login small-font" to="/login">
              已有账户登录
            </Link>
            {/* <span></span> */}
            <Link className="to-register small-font" to="/register">
            注册
            </Link>
            {/* <span className="to-register small-font">注册</span> */}
          </span>
        </div>
      </div>
    </div>
  );
}
