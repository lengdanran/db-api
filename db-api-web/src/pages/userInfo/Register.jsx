import React from 'react';
import Rcoder from '../../assets/image/Rcoder.jpg';
import './index.less';
import { Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Link } from 'umi';

export default function Register() {
  return (
    <div className="register">
      <div className="box">
        <div className="logo">
          <img className="logo-img" src={Rcoder} alt="logoImage" />
          <span className="logo-word">DBApi</span>
        </div>
        <span className="login-font">注册</span>
        <Input size="large" className="email-input" placeholder="邮箱" prefix={<UserOutlined />} />
        <Input.Password
          size="large"
          className="psd-input"
          placeholder="密码"
          prefix={<LockOutlined />}
        />
        <Button size="large" className="login-btn">
          注册
        </Button>

        <div style={{ textAlign: 'center' }}>
          <Link to="/login" className="small-font">
            已有账号登录
          </Link>
        </div>
      </div>
    </div>
  );
}
