import React from 'react';
import './index.less';
import Rcoder from '../../assets/image/Rcoder.jpg';
import wechat from '../../assets/image/wechat.jpg';
import { Input, Button, Card, Form, message } from 'antd';
import { useState } from 'react';
import { Link, router } from 'umi';
import { connect } from 'dva';

import { UserOutlined, LockOutlined, WechatOutlined, CloseOutlined } from '@ant-design/icons';

const Login = props => {
  const {
    login: {},
    dispatch,
  } = props;
  const [wechatVisible, setWechatVisible] = useState(false);
  const [form] = Form.useForm();

  const wechatLogin = () => {
    setWechatVisible(true);
  };
  const closeWechat = () => {
    setWechatVisible(false);
  };
  const forgetPsd = () => {
    console.log('forget');
  };

  const login = params => {
    let info = form.getFieldValue();
    console.log('info', info);
    dispatch({
      type: 'login/loginByName',
      payload: {
        username: info.username,
        password: info.password,
      },
    }).then(res => {
      if (res.success) {
        setTimeout(()=>{
          // localStorage.setItem("token", res.msg);
          sessionStorage.setItem("token", res.msg);
          router.push('/dataResource');
        },1000)
      }
    });
  };

  const onFinish = params => {};

  const onFinishFailed = params => {};

  return (
    <>
      {!wechatVisible && (
        <div className="login">
          <Form
            // name="basic"
            form={form}
            labelCol={{ span: 0 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <div className="box">
              <div className="logo">
                <img className="logo-img" src={Rcoder} alt="logoImage" />
                <span className="logo-word">DBApi</span>
              </div>
              <span className="login-font">登录</span>
              <Form.Item name="username">
                <Input
                  size="large"
                  className="email-input"
                  placeholder="用户名"
                  prefix={<UserOutlined />}
                />
              </Form.Item>

              <Form.Item name="password">
                <Input.Password
                  size="large"
                  className="psd-input"
                  placeholder="密码"
                  prefix={<LockOutlined />}
                />
              </Form.Item>

              <div>
                <Link to="/register" className="small-font">
                  注册账号
                </Link>
                <Link className="forget-psd small-font link" onClick={forgetPsd} to="/reset">
                  忘记密码
                </Link>
              </div>
              <Button size="large" className="login-btn" onClick={login}>
                登录
              </Button>
              <div className="wechat" style={{ textAlign: 'center' }} onClick={wechatLogin}>
                <WechatOutlined className="icon" />
                <span className="font small-font link">微信登录</span>
              </div>
            </div>
          </Form>
        </div>
      )}

      {wechatVisible && (
        <Card className="wechat-card">
          <CloseOutlined className="wechat-icon" onClick={closeWechat} />
          <h2 style={{ color: '#1890ff' }}>微信登录</h2>
          <img src={wechat} alt="wechatLogin" className="image" />
          <span className="tip">请使用微信扫描二维码登录</span>
        </Card>
      )}
    </>
  );
};

export default connect(({ login }) => ({ login }))(Login);
