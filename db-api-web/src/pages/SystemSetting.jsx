import React, {useEffect, useState} from 'react';
import {Button, Switch, Select, Divider} from 'antd';
import './index.less';
import '../assets/incofont.css';
import EditableTagGroup from './components/EditableTagGroup';
import {connect} from "dva";

function SystemSetting(props) {
  const {Option} = Select;
  const [appear, setAppear] = useState(true);
  const [whiteBlack, setWhiteBlack] = useState('black');
  const [ip, setIp] = useState([]);
  const [status, setStatus] = useState(true);
  const [firewallData, setFirewallData] = useState({});
  const {firewallManage: {}, dispatch} = props;

  function getFireWallInfo() {
    dispatch({
      type: "firewallManage/detail"
    }).then(res => {
      console.log("firewall", res);
      setStatus(res.status === 'on');
      setWhiteBlack(res.mode);
      setIp(res.mode === "black" ? res.blackIP.split("\n") : res.whiteIP === undefined ? [] : res.whiteIP.split("\n"));
      setFirewallData(res);
    });
  }

  useEffect(() => {
    console.log("组件加载时，运行一次该函数");
    getFireWallInfo();
  }, []);

  const handleChange = item => {
    setWhiteBlack(item);
  };

  const switchChange = item => {
    setAppear(item);
    setStatus(!status);
    dispatch({
      type: "firewallManage/save",
      payload: {
        mode: whiteBlack,
        blackIP: whiteBlack === 'black' ? ip.join("\n") : null,
        whiteIP: whiteBlack !== 'black' ? ip.join("\n") : null,
        status: !status ? "on" : "off"
      }
    }).then(() => {
      getFireWallInfo();
    });
  };

  function submit() {
    dispatch({
      type: "firewallManage/save",
      payload: {
        mode: whiteBlack,
        blackIP: whiteBlack === 'black' ? ip.join("\n") : null,
        whiteIP: whiteBlack !== 'black' ? ip.join("\n") : null,
        status: status ? "on" : "off"
      }
    }).then(() => {
      getFireWallInfo();
    });
  }

  return (
    <div className="system-setting">
      <h2 className="big-font">系统设置</h2>
      <div style={{marginBottom: '8px'}}>
        <h3 className="big-font inline-block ">IP防火墙设置</h3>
        <Switch
          className="switch"
          checkedChildren="ON"
          unCheckedChildren="OFF"
          onClick={switchChange}
          checked={status}
        />
      </div>
      <Divider/>
      <div style={{marginBottom: '8px'}} hidden={!status}>
        <Select
          className="select-bw"
          defaultValue={whiteBlack}
          style={{width: 120}}
          onChange={handleChange}
        >
          <Option value="black">黑名单</Option>
          <Option value="white">白名单</Option>
        </Select>
        <Button className="save" type="primary" onClick={submit}>
          确认保存
        </Button>
      </div>
      <h4 className="big-font ip-list" hidden={!status}>IP名单:</h4>
      {status && <EditableTagGroup className='edit-tag' whiteBlack={whiteBlack} setTags={setIp} tags={ip} type={'ip'}/>}
    </div>
  );
}

export default connect(({firewallManage}) => ({firewallManage}))(SystemSetting);
