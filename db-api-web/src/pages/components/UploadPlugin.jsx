import {Button, Divider, Form, Input, message, Modal, Upload} from "antd";
import React from "react";

export default function UploadPlugin(props) {
  const {vis, setVis, dispatch} = props;
  const [form] = Form.useForm();

  function handleOk() {
    form.validateFields().then(() => {
      let form_values = form.getFieldsValue();
      console.log("form_data", form_values);
      let files = form_values.files;
      files = files.fileList.map(({ originFileObj }) => originFileObj)
      const formData = new FormData();
      formData.append("name", form_values.name);
      formData.append("note", form_values.note == null ? "" : form_values.note);
      formData.append("classname", form_values.classname);
      formData.append("param_desc", form_values.param_desc == null ? "" : form_values.param_desc);
      files.forEach(file => formData.append('files', file));
      console.log("formData", formData);
      dispatch({
        type:"pluginManage/upload",
        payload: formData
      }).then((success) =>{
        if (!success) {
          message.error("插件上传异常")
        }
        setVis(!success);
      });
    })
  }

  function handleCancel() {
    setVis(false);
  }
  const onAddAttachment = ({ onSuccess }) => onSuccess();

  return (
    <Modal title={"上传插件"} visible={vis} onOk={handleOk} onCancel={handleCancel} width="800px">
      <Form form={form}>
        <Divider orientation={"left"} children={<h5 align={"center"} style={{color: "#1890ff"}}>插件信息</h5>}/>
        <Form.Item label="插件名称" name={"name"} rules={[{required: true, message: '请输入'}]}>
          <Input />
        </Form.Item>
        <Form.Item label="插件描述" name={"note"} rules={[{required: false, message: '请输入'}]}>
          <Input />
        </Form.Item>
        <Form.Item label="插件主类全类名" name={"classname"} rules={[{required: true, message: '请输入'}]}>
          <Input />
        </Form.Item>
        <Form.Item label="插件参数描述信息" name={"param_desc"} rules={[{required: false, message: '请输入'}]}>
          <Input />
        </Form.Item>
        <Form.Item label={"插件文件"} name={"files"}  rules={[{required: false, message: '请输入'}]}>
          <Upload customRequest={onAddAttachment}>
            <Button>添加</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  )
}
