import React, {useState} from 'react';
import {Modal, Button, DatePicker, Col, Row, Form, Input, Select, Space} from 'antd';
import './less/AddToken.less'
import {connect} from "dva";

function AddToken(props) {
  const {addModalVisible, setAddModalVisible, getAllTokens, tokenManage: {}, dispatch} = props;
  const [form] = Form.useForm();

  const handleCancel = () => {
    setAddModalVisible(false);
  };

  const onFinish = values => {
    console.log('Success:', values);
    dispatch({
      type: "tokenManage/addToken",
      payload: values
    }).then(res => {
      getAllTokens();
    });
    setAddModalVisible(false);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  function generateToken() {
    dispatch({
      type: "tokenManage/generateToken"
    }).then((res) => {
      form.setFieldsValue({token: res});
    });
  }

  function onDateChange(value) {
    form.setFieldsValue({
      expire: value.valueOf()
    });
  }

  function onDateOk(value) {
    onDateChange(value);
  }

  return (
    <Modal
      className='add-token'
      okText="确认"
      cancelText="取消"
      onCancel={handleCancel}
      footer={null}
      visible={addModalVisible}
      width="800px"
    >
      <div className="title">新增Token</div>
      <Form
        name='token-form'
        form={form}
        labelCol={{span: 8}}
        wrapperCol={{span: 16}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div className="basic-msg">
          <div className="title-font">Token信息</div>
          <Row gutter={16} className="form-item">
            <Col span={16}>
              <Form.Item
                label="Token"
                name="token"
                rules={[{required: true, message: '请输入Token或者自动生成'}]}
              >
                <Input/>
              </Form.Item>
            </Col>
            <Col span={4}>
              <Button onClick={generateToken}>自动生成</Button>
            </Col>
          </Row>
          <Row gutter={16} className="form-item">
            <Col span={16}>
              <Form.Item
                label="Token描述信息"
                name="note"
                rules={[{required: false}]}
              >
                <Input/>
              </Form.Item>
            </Col>
            <Col span={16}>
              <Form.Item
                label="过期时间"
                name="expire"
                rules={[{required: true, message: '过期时间必须选择'}]}
              >
                <Space direction="vertical" size={12}>
                  <DatePicker showTime onChange={onDateChange} onOk={onDateOk}/>
                </Space>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            wrapperCol={{
              offset: 18,
              span: 6,
            }}
          >
            <Button style={{marginRight: '10px', marginTop: '10px'}} onClick={handleCancel}>取消</Button>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Modal>
  )
}

export default connect(({tokenManage}) => ({tokenManage}))(AddToken);
