import {Upload, message, Button} from 'antd';
import {UploadOutlined} from '@ant-design/icons';
import "../../index.less"

export default function FileUpload(props) {

  const {url, token, getAllDataSource} = props;

  const ps = {
    name: 'file',
    action: url,//'http://127.0.0.1:8520/datasource/upload',
    headers: {
      Authorization: token//'authorization-text',
    },
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} 文件上传成功!`);
        getAllDataSource();
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 文件上传失败!`);
      }
    },
  };

  return (
    <Upload {...ps}>
      <Button className='btns' icon={<UploadOutlined/>}>导入配置文件</Button>
    </Upload>
  )
}
