import React, {useState} from 'react';
import {Modal, Button, Checkbox, Divider, Col, Row, Form, Input, message, Space} from 'antd';
import './less/AddToken.less'
/* eslint-disable */
export default function Authorization(props) {
  const {visible, setVisible, plainOptions, checkedOptions, setCheckedOptions, getAllTokens, tokenId, tokenManage: {}, dispatch} = props;
  const [form] = Form.useForm();
  const handleOk = () => {
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const CheckboxGroup = Checkbox.Group;

  // const plainOptions = ['Apple', 'Pear', 'Orange'];
  // const defaultCheckedList = ['Apple', 'Orange'];

  // const [checkedList, setCheckedList] = React.useState(defaultCheckedList);
  const [indeterminate, setIndeterminate] = React.useState(true);
  const [checkAll, setCheckAll] = React.useState(false);
  const [changed, setChanged] = React.useState(false);

  const onChange = list => {
    // setCheckedList(list);
    setCheckedOptions(list);
    setChanged(true);
    setIndeterminate(!!list.length && list.length < plainOptions.length);
    setCheckAll(list.length === plainOptions.length);
    console.log("checked", list.join(","));
    form.setFieldsValue({
      tokenId: tokenId,
      groupIds: list.join(",")
    });
  };

  const onCheckAllChange = e => {
    // TO FIX : 全选按钮有问题
    // setCheckedList(e.target.checked ? plainOptions : []);
    setCheckedOptions(e.target.checked ? plainOptions : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  const onFinish = values => {
    console.log('Success:', values);
    if (changed) {
      // TODO: 发送请求给服务器处理
      dispatch({
        type: "tokenManage/auth",
        payload: values
      }).then(res => {
        setCheckedOptions([]);
        getAllTokens();
      });
      message.success("授权成功");
    } else {
      message.success("没有任何修改");
    }
    setVisible(false);
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
    message.error("授权失败:", errorInfo);
  };

  return (
    <Modal
      className='add-token'
      okText="确认"
      cancelText="取消"
      footer={null}
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      width="800px"
    >
      <div className="title">授权该token访问以下分组的API</div>
      <br/>
      <Form
        form={form}
        labelCol={{span: 8}}
        wrapperCol={{span: 16}}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item name="tokenId" hidden={true}/>
        <Form.Item name="groupIds" hidden={true}/>
        <Checkbox indeterminate={indeterminate} onChange={onCheckAllChange} checked={checkAll}>
          全选
        </Checkbox>
        <Divider/>
        <CheckboxGroup options={plainOptions} value={checkedOptions} onChange={onChange}/>
        <Form.Item
          wrapperCol={{
            offset: 18,
            span: 6,
          }}
        >
          <Button style={{marginRight: '10px', marginTop: '10px'}} onClick={handleCancel}>取消</Button>
          <Button type="primary" htmlType="submit">
            确认
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
}
