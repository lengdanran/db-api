import React, {useEffect, useRef, useState} from 'react';
import {Modal, Button, Row, Col, Form, Input, Select} from 'antd';
import "./index.less"

export default function ApiInfoModal(props) {
  const {visible, setVisible, dispatch, api_id} = props;
  const [api, setApi] = useState({});
  const [datasourceName, setDatasourceName] = useState("");
  const [form] = Form.useForm();

  function getApiInfo() {
    dispatch({
      type: "apiManage/detail",
      payload: {
        id: api_id
      }
    }).then(res => {
      setApi(res);
      form.setFieldsValue(res);
      dispatch({
        type: "dataSource/getName",
        payload: {
          id: res.datasourceId
        }
      }).then(res => {
        setDatasourceName(res);
        form.setFieldsValue({datasourceName: res})
      });
    });
  }


  useEffect(() => {
    console.log("组件加载时，运行一次该函数");
    getApiInfo();
  }, []);


  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    handleCancel();
  };

  const handleCancel = () => {
    setVisible(false);
  };

  return (
    <Modal title={api.name + "信息"} visible={visible} onOk={handleOk} onCancel={handleCancel}>
      <Form layout="horizontal" form={form} initialValues={api}>
        <div className="basic-msg">
          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='id' label="API-ID" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='name' label="名称" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='note' label="描述" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='path' label="路径后缀" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='datasourceName' label="数据源" rules={[{isRequired: true}]}>
                <Input readOnly={true} value={datasourceName}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='params' label="参数" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='cachePlugin' label="缓存插件" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='cachePluginParams' label="缓存插件参数" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='transformPlugin' label="转换插件" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='transformPluginParams' label="转换插件参数" rules={[{isRequired: true}]}>
                <Input readOnly={true}/>
              </Form.Item>
            </Col>
          </Row>


        </div>


      </Form>
    </Modal>
  );
};
