import {Modal} from "antd";
import React from "react";

export default function TokenModal(props) {
  const {token, vis, setVis} = props;
  function handleOk() {
    setVis(false);
  }
  function handleCancel() {
    handleOk();
  }
  return (
    <Modal title={"Token"} visible={vis} onOk={handleOk} onCancel={handleCancel}>
      <h4 align={"center"} style={{color: "#1890ff"}}>{token}</h4>
    </Modal>
  )
}
