import React, {useImperativeHandle, forwardRef} from 'react'
import {Form, Input, Select, Row, Col} from 'antd'
import "../less/DataSourceForm.less"

function AddDataSourceForm(props, ref) {
  const {Option} = Select;
  const {data, edit, readOnly, getFormData} = props;
  const [form] = Form.useForm();
  useImperativeHandle(ref, () => ({
    // TODO: 请求发送
    getValue: () => {
      form.validateFields().then((values) => {
        console.log("values", values);
        getFormData(values);
      })
    }
  }));

  function onChange(value) {
    console.log(`selected ${value}`);
    form.setFieldsValue({type: value});
  }

  function onSearch(val) {
    console.log('search:', val);
  }

  return (
    <>
      <div className="title">{edit ? data["name"] + "数据源信息" : "新增数据源"}</div>
      <Form layout="horizontal" form={form} initialValues={edit ? data : {}}>
        <div className="basic-msg">
          <div className="title-font">基础信息</div>
          <Row gutter={16} className="form-item">
            <Col span={12}>
              <Form.Item name='type' label="数据库类型" rules={[{isRequired: true}]}>
                {readOnly && <Input defaultValue={data["type"]} readOnly={readOnly}/>}
                {!readOnly &&
                <Select
                  showSearch
                  placeholder="选择一种数据库"
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  defaultValue={edit ? data["type"] : {}}
                >
                  <Option value="MySQL">MySQL</Option>
                  <Option value="SQL Server">SQL Server</Option>
                  <Option value="Redis">Redis</Option>
                </Select>
                }


                {/* <Form.Item name='type' label="数据库类型" rules={[{ isRequired: true }]} >
                <Select
                  showSearch
                  placeholder="选择一种数据库"
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }

                >
                  <Option value="MySQL">MySQL</Option>
                  <Option value="SQL Server">SQL Server</Option>
                  <Option value="Redis">Redis</Option>
                </Select>
              </Form.Item> */}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='name' label="数据源名称" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='note' label="数据源描述" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='driver' label="JDBC Driver Class" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='url' label="JDBC URL" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={12}>
              <Form.Item name='username' label="用户名" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='password' label="密码" rules={[{isRequired: true}]}>
                <Input.Password readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='tableSql' label="获取数据表的SQL" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly} placeholder='这段SQL会被用来执行获取数据表操作'/>
              </Form.Item>
            </Col>
          </Row>
        </div>

      </Form>
    </>
  )
}

export default forwardRef(AddDataSourceForm)
