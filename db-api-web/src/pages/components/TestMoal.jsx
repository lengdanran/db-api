import Search from "antd/es/input/Search";
import {Col, Divider, Form, Input, Modal, Row, Select, Space} from "antd";
import React, {useEffect, useState} from "react";
import ReactJson from "react-json-view";
import axios from "axios";
import {LockOutlined, UnlockOutlined} from "@ant-design/icons";

export default function TestModal(props) {
  const {api, dispatch, vis, setVis, prefixPath} = props;
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const [apiData, setApiData] = useState({});
  const [params, setParams] = useState({});
  const [jsonData, setJsonData] = useState({});

  function getApi() {
    dispatch({
      type: "apiManage/detail",
      payload: {
        id: api.id
      }
    }).then(res => {
      setApiData(res);
    });
  }

  useEffect(() => {
    console.log("组件加载时，运行一次该函数");
    getApi();
  }, []);

  const d = {
    "id": "506fba12-0c16-44f3-8316-778a56883429",
    "name": "ddddd",
    "note": "dddddd",
    "path": "ddddd",
    "datasourceId": "92fe5035-44b0-44f3-8346-f14cb3bbc0e6",
    "sqlList": [
      {
        "id": 11,
        "apiId": "506fba12-0c16-44f3-8316-778a56883429",
        "sqlText": "asdgfhgj",
        "transformPlugin": null,
        "transformPluginParams": null
      }
    ],
    "params": "[{\"name\":\"sdagf\",\"type\":\"Array<bigint>\",\"note\":\"sdag\"}]",
    "status": 0,
    "previlege": 1,
    "groupId": "428490ce-fa32-48d4-accc-a9f6401db5ef",
    "cachePlugin": null,
    "cachePluginParams": null,
    "createTime": "2022-02-15 12:18:26",
    "updateTime": "2022-02-15 12:18:26",
    "transformPlugin": null,
    "transformPluginParams": null
  }

  function handleOk() {
    setVis(false);
  }

  function handleCancel() {
    handleOk();
  }

  function getParams() {
    form.validateFields().then(r => {
      console.log("r", r);
      setParams(form.getFieldsValue());
    });
  }

  function onSearch() {
    setLoading(true);
    form.validateFields().then(r => {
      console.log("r", r);
      setParams(form.getFieldsValue());
      const test_url = "http://127.0.0.1:8520/api/";
      const build_url = "http://121.4.195.203/server/api/"
      axios({
        method: 'get',
        params: r,
        url: build_url + api.path,
        headers: {
          "Authorization": r.token
        }
      }).then(r => {
        setJsonData(r.data);
        setLoading(false);
      });
    });

  }

  function createFormItem() {
    let params = JSON.parse(api.params);
    if (params === null) return ;
    return params.map((p) => {
      return (
        <Form.Item label={p.name} name={p.name} rules={[{required: true, message: '请输入'}]}>
          <Input addonAfter={p.type}/>
        </Form.Item>
      );
    });
  }

  return (
    <Modal title={"测试API"} visible={vis} onOk={handleOk} onCancel={handleCancel} width="800px">
      <Divider children={<h5 align={"center"} style={{color: "#1890ff"}}>测试API请求</h5>}/>
      <Search addonBefore={<>{apiData.previlege !== 0 ? <UnlockOutlined style={{color: "green"}}/> :
        <LockOutlined style={{color: "red"}}/>}{prefixPath}</>} value={api.path} placeholder="输入地址" enterButton="发送请求"
              size="large" loading={loading} onSearch={onSearch}/>
      <Divider orientation={"left"} children={<h5 align={"center"} style={{color: "#1890ff"}}>API请求参数</h5>}/>
      <Form form={form} title={"API请求参数"}>
        {createFormItem()}
        <Divider orientation={"left"} children={<h5 align={"center"} style={{color: "#1890ff"}}>API请求token</h5>}/>
        {api.previlege === 0 &&
        <Form.Item label="token" name={"token"} rules={[{required: true, message: '请输入'}]}>
          <Input addonAfter={"私有接口才需要输入Token"}/>
        </Form.Item>
        }
      </Form>
      <Divider orientation={"center"} children={<h5 align={"center"} style={{color: "#1890ff"}}>API接口响应</h5>}/>
      <ReactJson src={jsonData} indentWidth={2}/>
    </Modal>

  )
}

