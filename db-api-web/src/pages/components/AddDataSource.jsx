import React, {useState} from 'react';
import {Modal, Button, message, Row, Col, Input, Select} from 'antd';
// import './index.less';
import './less/DataSourceForm.less'
import {Form} from 'antd';
import {connect} from "dva";

function AddDataSource(props) {
  const {addModalVisible, setAddModalVisible, data, edit, readOnly, dataSource: {}, dispatch, getAllDataSource} = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [dis, setDis] = useState(true);
  const {Option} = Select;

  const handleOk = () => {
    setAddModalVisible(false);
    const formData = form.getFieldsValue();
    if (edit) {
      formData.id = data.id;
      dispatch({
        type: "dataSource/updateDataSource",
        payload: formData
      }).then((res) => {
        console.log(res);
        getAllDataSource();
      });
    } else {
      dispatch({
        type: "dataSource/addDataSource",
        payload: formData
      }).then((res) => {
        console.log(res);
        getAllDataSource();
      });
    }
  };

  const handleCancel = () => {
    setAddModalVisible(false);
  };

  const handleTest = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      message.success('连接成功');
      setDis(false);
    }, 3000);
  };

  function onChange(value) {
    console.log(`selected ${value}`);
    form.setFieldsValue({type: value});
  }

  function onSearch(val) {
    console.log('search:', val);
  }

  return (
    <Modal
      // title={edit ? data["name"] + "数据源" : "新增数据源"}
      className='add-data-source'
      okText="确认"
      cancelText="取消"
      visible={addModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      getContainer={false}
      width="800px"
      footer={[
        <Button key="back" onClick={handleCancel}>
          取消
        </Button>,
        <Button
          type="primary"
          loading={loading}
          onClick={handleTest}
          hidden={readOnly}
        >
          测试连接
        </Button>,
        <Button
          key="submit"
          type="primary"
          htmlType="submit"
          onClick={handleOk}
          hidden={readOnly}
          disabled={dis}>
          确定
        </Button>,

      ]}
    >
      <div className="title">{edit ? data["name"] + "数据源信息" : "新增数据源"}</div>
      <Form layout="horizontal" form={form} initialValues={edit ? data : {}}>
        <div className="basic-msg">
          <div className="title-font">基础信息</div>
          <Row gutter={16} className="form-item">
            <Col span={12}>
              <Form.Item name='type' label="数据库类型" rules={[{isRequired: true}]}>
                {readOnly && <Input defaultValue={data["type"]} readOnly={readOnly}/>}
                {!readOnly &&
                <Select
                  showSearch
                  placeholder="选择一种数据库"
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  defaultValue={edit ? data["type"] : {}}
                >
                  <Option value="MySQL">MySQL</Option>
                  <Option value="SQL Server">SQL Server</Option>
                  <Option value="Redis">Redis</Option>
                </Select>
                }
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='name' label="数据源名称" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='note' label="数据源描述" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='driver' label="JDBC Driver Class" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='url' label="JDBC URL" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={12}>
              <Form.Item name='username' label="用户名" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name='password' label="密码" rules={[{isRequired: true}]}>
                <Input.Password readOnly={readOnly}/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} className="form-item">
            <Col span={24}>
              <Form.Item name='tableSql' label="获取数据表的SQL" rules={[{isRequired: true}]}>
                <Input readOnly={readOnly} placeholder='这段SQL会被用来执行获取数据表操作'/>
              </Form.Item>
            </Col>
          </Row>
        </div>


      </Form>
      {/*<AddDataSourceForm ref={form} data={data} edit={edit} readOnly={readOnly} getFormData={getFormData}/>*/}
      {/* {edit ? <EditDataSourceForm ref={form} data={data}></EditDataSourceForm> : <AddDataSourceForm ref={form}></AddDataSourceForm>} */}
    </Modal>
  )
}

export default connect(({dataSource}) => ({dataSource}))(AddDataSource);
