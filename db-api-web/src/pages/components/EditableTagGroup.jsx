import {Tag, Input} from 'antd';
import {TweenOneGroup} from 'rc-tween-one';
import {PlusOutlined} from '@ant-design/icons';
import React from 'react';
import './index.less';

export default class EditableTagGroup extends React.Component {
  state = {
    tags: this.props.tags,
    inputVisible: false,
    inputValue: '',
  };

  handleClose = removedTag => {
    const tags = this.props.tags.filter(tag => tag !== removedTag);
    this.props.setTags(tags);
  };

  showInput = () => {
    this.setState({inputVisible: true}, () => this.input.focus());
  };

  handleInputChange = e => {
    this.setState({inputValue: e.target.value});
  };

  handleInputConfirm = () => {
    const {inputValue} = this.state;
    let {tags} = this.props;
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [...tags, inputValue];
    }
    console.log(tags);
    this.props.setTags(tags);
    this.setState({
      inputVisible: false,
      inputValue: '',
    });
  };

  saveInputRef = input => {
    this.input = input;
  };

  forMap = tag => {
    const tagElem = (
      <Tag
        style={{
          borderColor: this.props.type === 'ip' ? this.props.whiteBlack === 'black' ? 'red' : '#23d346' : '#1890ff',
          color: this.props.type === 'ip' ? this.props.whiteBlack === 'black' ? 'red' : '#23d346' : '#1890ff'
        }}
        closable
        onClose={e => {
          e.preventDefault();
          this.handleClose(tag);
        }}
      >
        {tag}
      </Tag>
    );
    return (
      <span key={tag} style={{display: 'inline-block'}}>
        {tagElem}
      </span>
    );
  };

  render() {
    const {inputVisible, inputValue} = this.state;
    const tagChild = this.props.tags.map(this.forMap);
    return (
      <div className="tags">
        <TweenOneGroup
          style={{display: 'inline-block'}}
          enter={{
            scale: 0.8,
            opacity: 0,
            type: 'from',
            duration: 100,
          }}
          onEnd={e => {
            if (e.type === 'appear' || e.type === 'enter') {
              e.target.style = 'display: inline-block';
            }
          }}
          leave={{opacity: 0, width: 0, scale: 0, duration: 200}}
          appear={false}
        >
          {tagChild}
        </TweenOneGroup>
        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="small"
            style={{width: 78}}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
          />
        )}
        {!inputVisible && (
          <Tag
            onClick={this.showInput}
            className="site-tag-plus"
            style={{
              borderColor: this.props.type === 'ip' ? this.props.whiteBlack === 'black' ? 'red' : '#23d346' : '#1890ff',
              color: this.props.type === 'ip' ? this.props.whiteBlack === 'black' ? 'red' : '#23d346' : '#1890ff'
            }}
          >
            <PlusOutlined/>
          </Tag>
        )}
      </div>
    );
  }
}
