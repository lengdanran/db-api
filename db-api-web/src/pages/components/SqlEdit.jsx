import React from 'react';
import AceEditor from 'react-ace';
// 引入对应的mode
import 'ace-builds/src-noconflict/mode-mysql';
// 引入对应的theme
import 'ace-builds/src-noconflict/theme-xcode';
// 如果要有代码提示，下面这句话必须引入!!!
import 'ace-builds/src-noconflict/ext-language_tools';
import './index.less';

export default function SqlEdit(props) {
  // const onChange = params => {};
  const {onChange} = props;
  const onPaste = params => {};
  const onLoad = params => {};
  return (
    <div className="editor">
      <AceEditor
        // ref="editorRef"
        style={{ height: '220px', width: '100%' }}
        placeholder="请输入sql语句"
        mode="mysql"
        theme="xcode"
        name="UNIQUE_ID_OF_DIV"
        fontSize={14}
        editorProps={{
          $blockScrolling: false,
        }}
        onChange={onChange}
        onPaste={onPaste}
        onLoad={onLoad}
        showPrintMargin={false}
        showGutter={true}
        highlightActiveLine={true}
        // 设置编辑器格式化和代码提示
        setOptions={{
          useWorker: false,
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          // 自动提词此项必须设置为true
          enableSnippets: true,
          showLineNumbers: false,
          tabSize: 2,
        }}
      />
    </div>
  );
}
