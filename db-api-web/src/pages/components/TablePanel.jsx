import React, { useState } from 'react';
import { TableOutlined } from '@ant-design/icons';

export default function TablePanel(props) {
  const { tableName, list } = props;
  const [isParamVisible, setIsParamVisible] = useState(true);
  const open = () => {
    // console.log('123');
    setIsParamVisible(!isParamVisible);
  };
  return (
    <div style={{ maginTop: '5px', marginBottom: '5px' }} onClick={open}>
      <TableOutlined />
      <span style={{ marginLeft: '10px' }}>{tableName}</span>
      <div style={{ display: isParamVisible ? 'none' : 'inline' }}>
        {list.map(item => {
          return <div style={{ marginLeft: '24px' }}>{item}</div>;
        })}
      </div>
    </div>
  );
}
