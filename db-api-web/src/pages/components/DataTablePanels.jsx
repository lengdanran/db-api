import TablePanel from "@/pages/components/TablePanel";
import React from "react";


export default function DataTablePanels(props) {
  const {data} = props;

  function createTablePanels() {
    let panels = data.map((item, index) => {
      let cols = Array();
      item.columns.forEach((t) => cols.push(t.label));
      return (<TablePanel tableName={item.label} list={cols}/>)
    });
    return (panels);
  }

  return (
    <div style={{overflow: 'scroll', height: '160px', overflowX: 'hidden'}}>
      {createTablePanels()}
    </div>
  )
}
