// import { Redirect } from 'umi'
import { Redirect } from 'dva/router'
export default (props) => {
  console.log("Storage token", sessionStorage.getItem("token"));
  const { isLogin } = window.localStorage.getItem("token") !== null;
  if (isLogin) {
    return <>{ props.children }</>;
  } else {
    return <Redirect to="/login" />;
  }
}
