import {message} from "antd";
import {getAllTables} from "@/services/tableManage";

const TableMangeModel = {
  namespace: 'tableManage',
  state: {},
  effects: {
    * getAllTables({payload}, {call, put}) {
      const res = yield call(getAllTables, payload);
      if (res) {
        message.success('数据表获取成功');
      } else {
        message.error("数据表获取失败");
      }
      return res;
    },

  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default TableMangeModel;
