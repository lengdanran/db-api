import {message} from 'antd';
import {
  apiDocs, detail,
  downloadConfig,
  downloadGroupConfig,
  getAllApiConfigInfo,
  getApiConfigInfo,
  getGroups, getIPPort, offline, online, getToken, del, add
} from '@/services/apiManage';

const ApiMangeModel = {
  namespace: 'apiManage',
  state: {},
  effects: {
    * getApiConfigInfo({payload}, {call, put}) {
      const res = yield call(getApiConfigInfo, payload);
      if (res) {
        message.success("获取api配置信息成功")
      } else {
        message.error('获取api配置信息失败');
      }
      return res;
    },
    * getGroups({payload}, {call, put}) {
      const res = yield call(getGroups, payload);
      if (res) {
        message.success("获取分组成功");
      } else {
        message.error('获取分组失败');
      }
      return res;
    },
    * getAllApiConfigInfo({payload}, {call, put}) {
      return yield call(getAllApiConfigInfo, payload);
    },
    * apiDocs({payload}, {call, put}) {
      return yield call(apiDocs, payload);
    },
    * downloadConfig({payload}, {call, put}) {
      return yield call(downloadConfig, payload);
    },
    * downloadGroupConfig({payload}, {call, put}) {
      return yield call(downloadGroupConfig, payload);
    },
    * detail({payload}, {call, put}) {
      return yield call(detail, payload);
    },
    * online({payload}, {call, put}) {
      return yield call(online, payload);
    },
    * offline({payload}, {call, put}) {
      return yield call(offline, payload);
    },
    * getIPPort({payload}, {call, put}) {
      return yield call(getIPPort, payload);
    },
    * getToken({payload}, {call, put}) {
      return yield call(getToken, payload);
    },
    * del({payload}, {call, put}) {
      return yield call(del, payload);
    },
    * add({payload}, {call, put}) {
      return yield call(add, payload);
    },
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default ApiMangeModel;
