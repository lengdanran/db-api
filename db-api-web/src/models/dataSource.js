import {message} from "antd";
import {
  addDataSource,
  deleteDataSource,
  exportDataSource,
  getAllDataSource,
  getDataSourceInfo,
  getName,
  updateDataSource
} from "@/services/dataSource";


const DataSourceModel = {
  namespace: 'dataSource',
  state: {},
  effects: {
    * getDataSourceInfo({payload}, {call, put}) {
      const res = yield call(getDataSourceInfo, payload);
      if (res) {
        yield put({
          type: "dataSourceInfo",
          payload: res
        });
      } else {
        message.error('获取数据源信息失败:不存在该数据源信息');
      }
      return res;
    },
    * addDataSource({payload}, {call, put}) {
      const res = yield call(addDataSource, payload);
      if (res) {
        yield put({
          type: "addDataSource",
          payload: res
        });
      } else {
        message.success('数据源添加成功');
      }
      return res;
    },
    * updateDataSource({payload}, {call, put}) {
      const res = yield call(updateDataSource, payload);
      if (res) {
        message.success('数据源更新成功');
      } else {
      }
      return res;
    },
    * getAllDataSource({payload}, {call, put}) {
      const res = yield call(getAllDataSource, payload);
      if (res) {
        message.success('获取数据源信息成功');
      } else {
        message.error('获取数据源信息失败');
      }
      return res;
    },
    * deleteDataSource({payload}, {call, put}) {
      const res = yield call(deleteDataSource, payload);
      if (res) {
        message.success('删除数据源信息成功');
      } else {
        message.error('删除数据源信息失败');
      }
      return res;
    },
    * exportDataSource({payload}, {call, put}) {
      const res = yield call(exportDataSource, payload);
      if (res) {
        message.success('导出成功');
      } else {
        message.error('导出失败');
      }
      return res;
    },
    * getName({payload}, {call, put}) {
      return yield call(getName, payload);
    }
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

export default DataSourceModel;
