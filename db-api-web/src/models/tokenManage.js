import {message} from "antd";
import {addToken, auth, deleteToken, generateToken, getAllTokens, getAuthGroups} from "@/services/tokenManage";

const TokenMangeModel = {
  namespace: 'tokenManage',
  state: {},
  effects: {
    * generateToken({payload}, {call, put}) {
      const res = yield call(generateToken, payload);
      if (res) {
        message.success('Token生成成功');
      } else {
        message.error("Token生成失败");
      }
      return res;
    },
    * addToken({payload}, {call, put}) {
      const res = yield call(addToken, payload);
      if (!res) {
        message.success('Token添加成功');
      } else {
        message.error("Token添加失败");
      }
      return res;
    },
    * auth({payload}, {call, put}) {
      const res = yield call(auth, payload);
      if (!res) {
        message.success('Token授权成功');
      } else {
        message.error("Token授权失败");
      }
      return res;
    },
    * getAllTokens({payload}, {call, put}) {
      const res = yield call(getAllTokens, payload);
      if (res) {
        message.success('Token获取成功');
      } else {
        message.error("Token获取失败");
      }
      return res;
    },
    * deleteToken({payload}, {call, put}) {
      const res = yield call(deleteToken, payload);
      if (res) {
        message.error('删除失败');
      } else {
        message.success("删除成功");
      }
      return res;
    },
    * getAuthGroups({payload}, {call, put}) {
      const res = yield call(getAuthGroups, payload);
      if (res) {
        message.success('Token获取成功');
      } else {
        message.error("Token获取失败");
      }
      return res;
    },
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default TokenMangeModel;
