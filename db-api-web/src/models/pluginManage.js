
import {message} from "antd";
import {upload} from "@/services/pluginManage";

const PluginManageModel = {
  namespace: 'pluginManage',
  state: {},
  effects: {
    * upload({payload}, {call, put}) {
      const res = yield call(upload, payload);
      if (res.success) {
        message.success(res.msg);
      } else {
        message.error(res.msg);
      }
      return res.success;
    }
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default PluginManageModel;
