import {message} from 'antd';
import {createGroups, getAll, create} from "@/services/groupManage";

const GroupManageModel = {
  namespace: 'groupManage',
  state: {},
  effects: {
    * getAll({payload}, {call, put}) {
      const res = yield call(getAll, payload);
      if (res) {
        message.success("获取分组成功");
      } else {
        message.error("获取分组失败");
      }
      return res;
    },
    * createGroups({payload}, {call, put}) {
      const res = yield call(createGroups, payload);
      if (!res) {
        message.success("创建分组成功");
      } else {
        message.error("创建分组失败");
      }
      return res;
    },
    * create({payload}, {call, put}) {
      const res = yield call(create, payload);
      if (!res) {
        message.success("创建分组成功");
      } else {
        message.error("创建分组失败");
      }
      return res;
    },
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default GroupManageModel;
