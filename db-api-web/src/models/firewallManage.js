import {message} from "antd";
import {detail, save} from "@/services/firewallManage";

const FirewallMangeModel = {
  namespace: 'firewallManage',
  state: {},
  effects: {
    * save({payload}, {call, put}) {
      const res = yield call(save, payload);
      if (!res) {
        message.success('防火墙保存成功');
      } else {
        message.error("防火墙保存成功");
      }
      return res;
    },
    * detail({payload}, {call, put}) {
      const res = yield call(detail, payload);
      if (res) {
        message.success('获取防火墙信息成功');
      } else {
        message.error("获取防火墙信息失败");
      }
      return res;
    },
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
export default FirewallMangeModel;
