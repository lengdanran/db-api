import request from '../utils/request';

// 获取分组信息
export async function getAll() {
  return request("/group/getAll", {
    method: "GET"
  });
}

// 创建分组-list
export async function createGroups(data) {
  return request("/group/createGroups", {
    method: "POST",
    params: data
  });
}

// 创建分组-list
export async function create(data) {
  return request("/group/create", {
    method: "POST",
    params: data
  });
}
