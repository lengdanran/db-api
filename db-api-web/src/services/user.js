import request from '@/utils/request';

export async function loginByName(data) {
  return request('/user/login', {
    method: 'POST',
    params: data,
  });
}
