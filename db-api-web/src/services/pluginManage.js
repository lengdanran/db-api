import request from "@/utils/request"

export async function upload(data) {
  return request("/plugin/upload", {
    method: "POST",
    data: data,
    requestType: 'form'
  });
}
