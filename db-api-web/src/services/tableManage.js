import request from "@/utils/request";

// 获取对应数据源的所有数据表信息
export async function getAllTables(data) {
  return request("/table/getAllTables", {
    method: 'GET',
    params: data
  });
}
