import request from '../utils/request';

/**
 * 获取数据源信息
 * @param data
 * @returns {Promise<*>}
 */
export async function getDataSourceInfo(data) {
  return request("/datasource/detail/" + data.id, {
    method: "GET"
  });
}

/**
 * 添加数据源
 * @param data
 * @returns {Promise<*>}
 */
export async function addDataSource(data) {
  return request("/datasource/add", {
    method: "POST",
    params: data
  });
}

// 更新数据源
export async function updateDataSource(data) {
  return request("/datasource/update", {
    method: "POST",
    params: data
  });
}

// 获取所有数据源
export async function getAllDataSource() {
  return request("/datasource/getAll", {
    method: "GET"
  });
}

// 删除数据源
export async function deleteDataSource(data) {
  return request("/datasource/delete/" + data.id, {
    method: "POST"
  });
}

// 导出选择的数据源配置文件
export async function exportDataSource(data) {
  return request("/datasource/export", {
    method: "POST",
    params: data
  });
}

export async function getName(data) {
  return request("/datasource/getName", {
    method: "post",
    params: data
  });
}
