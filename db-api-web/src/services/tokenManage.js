import request from '../utils/request';

// 自动生成token令牌
export async function generateToken() {
  return request("/token/generate", {
    method: "GET"
  });
}

// 添加token
export async function addToken(data) {
  return request("/token/add", {
    method: "POST",
    params: data
  });
}

// token 授权
export async function auth(data) {
  return request("/token/auth", {
    method: "POST",
    params: data
  });
}

// 获取所有的token
export async function getAllTokens() {
  return request(("/token/getAll"), {
    method: "GET"
  });
}

// 获取已经授权的分组
export async function getAuthGroups(data) {
  return request("/token/getAuthGroups/" + data.id, {
    method: "GET"
  });
}

// 删除token
export async function deleteToken(data) {
  return request("/token/delete/" + data.id, {
    method: "POST"
  });
}
