import request from "@/utils/request";

// 获取防火墙信息
export async function detail() {
  return request("/firewall/detail", {
    method: "GET"
  });
}

// 保存防火墙信息
export async function save(data) {
  return request("/firewall/save", {
    method: "POST",
    params: data
  });
}
