import request from '@/utils/request';
import download from "@/utils/download";

// 根据apiID获取api信息
export async function getApiConfigInfo(data) {
  return request('/apiConfig/detail/' + data["id"], {
    method: 'GET',
  });
}

// 获取API分组信息
export async function getGroups() {
  return request('/group/getAll', {
    method: 'GET'
  });
}

// 获取所有的api配置
export async function getAllApiConfigInfo() {
  return request("/apiConfig/getAll", {
    method: "GET"
  });
}

export async function apiDocs() {
  return download("/apiConfig/apiDocs", {
    method: "GET"
  });
}

export async function downloadConfig() {
  return download("/apiConfig/downloadConfig", {
    method: "GET"
  });
}

export async function downloadGroupConfig() {
  return download("/apiConfig/downloadGroupConfig", {
    method: "GET"
  });
}

export async function detail(data) {
  return request("/apiConfig/detail/" + data.id, {
    method: "GET"
  });
}

export async function online(data) {
  return request("/apiConfig/online/" + data.id, {
    method: "POST"
  });
}

export async function offline(data) {
  return request("/apiConfig/offline/" + data.id, {
    method: "POST"
  });
}

export async function getIPPort() {
  return request("/apiConfig/getIPPort", {
    method: "GET"
  });
}

export async function getToken(data) {
  return request("/apiConfig/getToken", {
    method: "GET",
    params: data
  });
}

export async function del(data) {
  return request("/apiConfig/delete/" + data.id, {
    method: "POST"
  });
}

export async function add(data) {
  return request("/apiConfig/add", {
    method: 'POST',
    params: data
  });
}
