// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  // build 配置
  publicPath: './',
  history: 'hash',
  hash: true,
  routes: [
    {
      path: '/login',
      component: '../pages/userInfo/Login',
    },
    {
      path: '/reset',
      component: '../pages/userInfo/ResetPsd',
    },
    {
      path: '/register',
      component: '../pages/userInfo/Register',
    },
    {
      path: '/',
      wrappers: ['@/pages/wrappers/auth',],
      component: '../layouts',
      routes: [
        { path: '/', component: '../pages/userInfo/Login' },
        {
          path: '/dataResource',
          component: '../pages/DataResource',
        },
        {
          path: '/apiManage',
          component: '../pages/ApiManage',
        },
        {
          path: '/userPrivilege',
          component: '../pages/UserPrivilege',
        },
        {
          path: '/systemSetting',
          component: '../pages/SystemSetting',
        },
      ],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        // dva: true,
        dva: {
          disableModelsReExport: true,
          lazyLoad: true,
          immer: true,
          hmr: false,
        },
        dynamicImport: false,
        title: 'DBApi',
        dll: false,

        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],

  cssLoaderOptions: {
    localIdentName: '[local]', // 配置这行
  },
};
