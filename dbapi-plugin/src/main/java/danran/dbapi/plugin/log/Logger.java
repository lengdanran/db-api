package danran.dbapi.plugin.log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Classname Logger
 * @Description TODO
 * @Date 2022/1/14 14:39
 * @Created by RanCoder
 */
public class Logger {
    private  Class<?> clazz;
    private Logger(Class<?> clazz) {
        this.clazz = clazz;
    }

    public static Logger getInstance(Class<?> className) {
        return new Logger(className);
    }

    public void info(String msg) {
        System.out.println("[" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\t" + clazz.getName() +"]:\t\t" + msg);
    }

    public static void main(String[] args) {
        Logger log = Logger.getInstance(Logger.class);
        log.info("hello");
    }
}
