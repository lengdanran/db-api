package danran.dbapi.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PluginConf {

    private static Properties pluginConfPro = new Properties();
    private static Properties p = new Properties();

    static {
        try {
            InputStream pluginConfProIn = PluginConf.class.getClassLoader().getResourceAsStream("PluginConf.properties");
            pluginConfPro.load(pluginConfProIn);
            String plugin_properties_path = pluginConfPro.getProperty("plugin.properties.path");
            System.out.println("<<===Get the plugin_properties_path===>>>:" + plugin_properties_path);
            InputStream in = PluginConf.class.getClassLoader().getResourceAsStream(plugin_properties_path);
            p.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String getKey(String key) {
        return p.getProperty(key);
    }

}
