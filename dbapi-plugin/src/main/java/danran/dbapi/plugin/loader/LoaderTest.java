package danran.dbapi.plugin.loader;

import com.alibaba.fastjson.JSONObject;
import danran.dbapi.plugin.TransformerPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Classname LoaderTest
 * @Description TODO
 * @Date 2022/2/8 20:42
 * @Created by RanCoder
 */
public class LoaderTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        PluginClassLoader loader = new PluginClassLoader(".");
        loader.addJar("E:\\desktop\\t\\dbapi-plugin-demo\\target\\dbapi-plugin-demo-1.0.0.jar");
        Class<?> clazz = loader.loadClass("demo.EncryptTransformerPlugin");
//        Class<?> clazz = Class.forName("demo.EncryptTransformerPlugin");
        TransformerPlugin instance = (TransformerPlugin)clazz.newInstance();
        List<JSONObject> d = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("num", "123456789");
        d.add(jsonObject);
        instance.transform(d, "num");
        System.out.println(d.get(0).toJSONString());
    }
}
