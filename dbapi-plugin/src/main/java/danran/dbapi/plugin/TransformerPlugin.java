package danran.dbapi.plugin;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

public abstract class TransformerPlugin {


    /**
     * 数据转换逻辑
     * @param data sql查询结果
     * @param params 缓存插件局部参数
     * @return
     */
    public abstract Object transform(List<JSONObject> data, String params);
}
