package danran.dbapi.common;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;

/**
 * @Classname ApiConfig
 * @Description TODO
 * @Date 2022/1/12 21:17
 * @Created by RanCoder
 */
@TableName("api_config")
public class ApiConfig {
    @TableId(value = "id")
    String id;

    @TableField
    String name;

    @TableField
    String note;

    @TableField
    String path;

    @TableField(value = "datasource_id")
    String datasourceId;

    @TableField(exist = false)
    List<ApiSql> sqlList;

    @TableField
    String params;

    @TableField
    Integer status;

    @TableField
    Integer previlege;

    @TableField(value = "group_id")
    String groupId;

    @TableField(value = "cache_plugin")
    String cachePlugin;

    @TableField(value = "cache_plugin_params")
    String cachePluginParams;

    @TableField(value = "create_time")
    String createTime;

    @TableField(value = "update_time")
    String updateTime;

    @TableField(value = "transform_plugin")
    String transformPlugin;

    @TableField(value = "transform_plugin_params")
    String transformPluginParams;

    public ApiConfig() {
    }

    public ApiConfig(String id, String name, String note, String path,
                     String datasourceId, List<ApiSql> sqlList, String params,
                     Integer status, Integer previlege, String groupId,
                     String cachePlugin, String cachePluginParams,
                     String transform_plugin, String transform_plugin_params,
                     String createTime, String updateTime) {
        this.id = id;
        this.name = name;
        this.note = note;
        this.path = path;
        this.datasourceId = datasourceId;
        this.sqlList = sqlList;
        this.params = params;
        this.status = status;
        this.previlege = previlege;
        this.groupId = groupId;
        this.cachePlugin = cachePlugin;
        this.cachePluginParams = cachePluginParams;
        this.transformPlugin = transform_plugin;
        this.transformPluginParams = transform_plugin_params;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getTransformPlugin() {
        return transformPlugin;
    }

    public ApiConfig setTransformPlugin(String transformPlugin) {
        this.transformPlugin = transformPlugin;
        return this;
    }

    public String getTransformPluginParams() {
        return transformPluginParams;
    }

    public ApiConfig setTransformPluginParams(String transformPluginParams) {
        this.transformPluginParams = transformPluginParams;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDatasourceId() {
        return datasourceId;
    }

    public void setDatasourceId(String datasourceId) {
        this.datasourceId = datasourceId;
    }

    public List<ApiSql> getSqlList() {
        return sqlList;
    }

    public void setSqlList(List<ApiSql> sqlList) {
        this.sqlList = sqlList;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPrevilege() {
        return previlege;
    }

    public void setPrevilege(Integer previlege) {
        this.previlege = previlege;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCachePlugin() {
        return cachePlugin;
    }

    public void setCachePlugin(String cachePlugin) {
        this.cachePlugin = cachePlugin;
    }

    public String getCachePluginParams() {
        return cachePluginParams;
    }

    public void setCachePluginParams(String cachePluginParams) {
        this.cachePluginParams = cachePluginParams;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ApiConfig{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", note='" + note + '\'' +
                ", path='" + path + '\'' +
                ", datasourceId='" + datasourceId + '\'' +
                ", sqlList=" + sqlList +
                ", params='" + params + '\'' +
                ", status=" + status +
                ", previlege=" + previlege +
                ", groupId='" + groupId + '\'' +
                ", cachePlugin='" + cachePlugin + '\'' +
                ", cachePluginParams='" + cachePluginParams + '\'' +
                ", transform_plugin='" + transformPlugin + '\'' +
                ", transform_plugin_params='" + transformPluginParams + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
