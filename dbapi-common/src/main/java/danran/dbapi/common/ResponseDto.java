package danran.dbapi.common;

import java.sql.ResultSet;

/**
 * @Classname ResponseDto
 * @Description 响应包装类
 * @Date 2022/1/12 21:31
 * @Created by RanCoder
 */
public class ResponseDto {
    String msg;
    Object data;
    Boolean success;

    public ResponseDto() {
    }

    public String getMsg() {
        return msg;
    }

    public ResponseDto setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getData() {
        return data;
    }

    public ResponseDto setData(Object data) {
        this.data = data;
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public ResponseDto setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public static ResponseDto apiSuccess(Object data) {
        return new ResponseDto().setData(data).setSuccess(true).setMsg("Api access succeeded");
    }

    public static ResponseDto successWithData(Object data) {
        return new ResponseDto().setData(data).setSuccess(true);
    }

    public static ResponseDto successWithMsg(String msg) {
        return new ResponseDto().setData(null).setSuccess(true).setMsg(msg);
    }

    public static ResponseDto fail(String msg) {
        return new ResponseDto().setMsg(msg).setSuccess(false).setData(null);
    }
}
