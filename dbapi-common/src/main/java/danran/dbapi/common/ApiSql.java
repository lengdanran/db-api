package danran.dbapi.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Classname ApiSql
 * @Description TODO
 * @Date 2022/1/12 21:28
 * @Created by RanCoder
 */
@TableName(value = "api_sql")
public class ApiSql {

    @TableId(type = IdType.AUTO)
    Integer id;

    @TableField("api_id")
    String apiId;

    @TableField("sql_text")
    String sqlText;

    @TableField(value = "transform_plugin")
    String transformPlugin;

    @TableField(value = "transform_plugin_params")
    String transformPluginParams;

    public ApiSql(String apiId, String sql) {
        this.apiId = apiId;
        this.sqlText = sql;
    }

    public ApiSql() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getSqlText() {
        return sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public String getTransformPlugin() {
        return transformPlugin;
    }

    public void setTransformPlugin(String transformPlugin) {
        this.transformPlugin = transformPlugin;
    }

    public String getTransformPluginParams() {
        return transformPluginParams;
    }

    public void setTransformPluginParams(String transformPluginParams) {
        this.transformPluginParams = transformPluginParams;
    }
}
