package danran.dbapi.core.token;

public interface TokenHandler {

    public String handleToken(String content);
}
