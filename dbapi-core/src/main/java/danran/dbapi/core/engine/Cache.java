package danran.dbapi.core.engine;



import danran.dbapi.core.node.SqlNode;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存类，内部是<code>ConcurrentHashMap</code>
 * @Classname Cache
 * @Description TODO
 * @Date 2022/1/14 13:40
 * @Created by RanCoder
 */
public class Cache {

    ConcurrentHashMap<String, SqlNode> nodeCache = new ConcurrentHashMap<>();

    public ConcurrentHashMap<String, SqlNode> getNodeCache() {
        return nodeCache;
    }
}
