package danran.dbapi.core.handler;

import danran.dbapi.core.node.IfSqlNode;
import danran.dbapi.core.node.MixedSqlNode;
import danran.dbapi.core.node.SqlNode;
import org.dom4j.Element;

import java.util.List;


public class IfHandler implements TagHandler {

    @Override
    public void handle(Element element, List<SqlNode> targetContents) {
        String test = element.attributeValue("test");
        if (test == null) {
            throw new RuntimeException("<if> handler missing test attribute");
        }

        List<SqlNode> contents = XmlParser.parseElement(element);

        IfSqlNode ifSqlNode = new IfSqlNode(test, new MixedSqlNode(contents));
        targetContents.add(ifSqlNode);

    }
}
