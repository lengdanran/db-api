package danran.dbapi.core.handler;

import danran.dbapi.core.node.MixedSqlNode;
import danran.dbapi.core.node.SqlNode;
import danran.dbapi.core.node.WhereSqlNode;
import org.dom4j.Element;

import java.util.List;


public class WhereHandler implements TagHandler{

    @Override
    public void handle(Element element, List<SqlNode> targetContents) {
        List<SqlNode> contents = XmlParser.parseElement(element);

        WhereSqlNode node = new WhereSqlNode(new MixedSqlNode(contents));
        targetContents.add(node);
    }
}
