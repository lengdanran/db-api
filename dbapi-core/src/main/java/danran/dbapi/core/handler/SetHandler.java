package danran.dbapi.core.handler;

import danran.dbapi.core.node.MixedSqlNode;
import danran.dbapi.core.node.SetSqlNode;
import danran.dbapi.core.node.SqlNode;
import org.dom4j.Element;

import java.util.List;


public class SetHandler implements TagHandler{

    @Override
    public void handle(Element element, List<SqlNode> targetContents) {
        List<SqlNode> contents = XmlParser.parseElement(element);

        SetSqlNode node = new SetSqlNode(new MixedSqlNode(contents));
        targetContents.add(node);
    }
}
