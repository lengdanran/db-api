package danran.dbapi.core.node;



import danran.dbapi.core.context.Context;

import java.util.Set;


public interface SqlNode {

    void apply(Context context);

    void applyParameter(Set<String> set);

}
