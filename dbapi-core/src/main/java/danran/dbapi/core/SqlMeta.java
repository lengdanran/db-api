package danran.dbapi.core;

import java.util.List;

/**
 * Sql元数据，包含sql语句以及对应的参数值列表，参数顺序与参数值列表顺序一致
 *
 * @Classname SqlMeta
 * @Description TODO
 * @Date 2022/1/14 13:10
 * @Created by RanCoder
 */
public class SqlMeta {

    String sql;
    List<Object> jdbcParamValues;

    public SqlMeta(String sql, List<Object> jdbcParamValues) {
        this.sql = sql;
        this.jdbcParamValues = jdbcParamValues;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<Object> getJdbcParamValues() {
        return jdbcParamValues;
    }

    public void setJdbcParamValues(List<Object> jdbcParamValues) {
        this.jdbcParamValues = jdbcParamValues;
    }
}
