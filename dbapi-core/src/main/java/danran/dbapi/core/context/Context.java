package danran.dbapi.core.context;


import danran.dbapi.core.util.OgnlUtil;

import java.util.*;


/***
 * 包含sql语句以及该sql对应的参数名以及对应的参数名的列表
 *
 * @Classname Context
 * @Description TODO
 * @Date 2022/1/14 13:20
 * @Created by RanCoder
 */
public class Context {
    // sql语句生成器
    private StringBuilder sqlBuilder = new StringBuilder();
    // sql动态参数值列表
    private final List<Object> jdbcParameters = new ArrayList<>();
    // 参数名称
    private final Set<String> paramNames = new HashSet<>();

    protected Map<String, Object> data;

    public Context(Map<String, Object> data) {
        this.data = data;
    }

    public void appendSql(String text) {
        if (text != null) {
            sqlBuilder.append(text);
        }
    }

    /**
     * 往参数值列表中添加一个新的值
     *
     * @param o 需要添加的值
     */
    public void addParameter(Object o) {
        jdbcParameters.add(o);
    }

    /**
     * 往参数名称列表中添加一个新的参数名称
     *
     * @param parameterName 参数名称
     */
    public void addParameterName(String parameterName) {
        paramNames.add(parameterName);
    }

    /**
     * 通过ognl表达式获取值
     *
     * @param expression 表达式
     * @return 值
     */
    public Object getOgnlValue(String expression) {
        return OgnlUtil.getValue(expression, data);
    }

    /**
     * 通过ognl表达式获取boolean值
     *
     * @param expression 表达式
     * @return boolean值
     */
    public Boolean getOgnlBooleanValue(String expression) {
        return OgnlUtil.getBooleanValue(expression, data);
    }

    /**
     * @return sql字符串
     */
    public String getSql() {
        return sqlBuilder.toString();
    }

    public Context setSql(String text) {
        sqlBuilder = new StringBuilder(text);
        return this;
    }

    public List<Object> getJdbcParameters() {
        return jdbcParameters;
    }

    public Map<String, Object> getData() {
        return data;
    }

}
