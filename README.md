# DB-API——低代码数据库数据接口系统

> 当前只支持MySQL数据库

## 项目简介

- 可以通过只编写对应的SQL等数据查询语句（当前只支持MySQL), 即可生成对应数据接口API，用户可以通过API访问得到对应的数据
- 线上体验地址：http://121.4.195.203/ 用户名：admin 密码：admin
- 项目截图
- ![image-20220218141705171](README.assets/image-20220218141705171.png)

## 技术选型

### 后端——dev分支

- Java 12
- MySQL
- Spring Boot 2.3.7
- mybatis-plus 3.2.0

### 前端——ui-dev分支

- React
- Umi
- dva

## 项目特点优势

- 开箱即用，依赖项少，系统运行只需要java，node环境即可
- 支持SQL语句获取数据直接生成
- 支持API权限控制，分组管理
- 支持自定义插件，通过上传对应的插件文件，系统即可动态加载插件，不需要重启系统
- 支持各项配置信息导入导出
- 支持动态SQL, 语法和mybatis类似
- 支持动态管理API，上线下线，在线测试

## 系统截图

![image-20220218143353064](README.assets/image-20220218143353064.png)

![image-20220218143541889](README.assets/image-20220218143541889.png)

![image-20220218143628717](README.assets/image-20220218143628717.png)

![image-20220218143704296](README.assets/image-20220218143704296.png)

![image-20220218143726707](README.assets/image-20220218143726707.png)







